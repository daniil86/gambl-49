import gsap from 'gsap';
import { deleteMoney, noMoney, getRandom, addMoney } from '../files/functions.js';

if (sessionStorage.getItem('money')) {
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
} else {
	sessionStorage.setItem('money', 12000);
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}

let tl = gsap.timeline({ defaults: { ease: "Power1.easeInOut", duration: 1.5 } });

//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-anim img');
function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (document.querySelector('.icon-anim img')) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}

const btnHome = document.querySelector('.controls-slot__btn-home');
function getRandomAnimate2() {
	let number = getRandom(1, 3);

	if (btnHome.classList.contains('_anim-1')) {
		btnHome.classList.remove('_anim-1');
	} else if (btnHome.classList.contains('_anim-2')) {
		btnHome.classList.remove('_anim-2');
	}

	setTimeout(() => {
		btnHome.classList.add(`_anim-${number}`);
	}, 100);
}

if (btnHome) {
	setInterval(() => {
		getRandomAnimate2();
	}, 20000);
}

// Функция-предохранитель - если в банке будет очень много денег, уменьшает шрифт текста
function checkLengthScore() {
	const score = document.querySelector('.score-box__score');
	let length = score.clientWidth;
	if (length > 90) {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		score.classList.add('_txt-small');
	}
	else if (length > 70 && length <= 90) {
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
		score.classList.add('_txt-middle');
	}
	else {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
	}
}

//========================================================================================================================================================
if (document.querySelector('.wrapper')) checkLengthScore();

//========================================================================================================================================================
const configSlot = {
	currentWin: 0,
	winCoeff_1: 20,
	winCoeff_2: 50,
	bet: 100,
	isWin: false
}
const configGSAP = {
	duration_1: 2.5,
	duration_2: 2
}

//game-1
if (document.querySelector('.slot__body')) {
	document.querySelector('.slot__body').classList.add('_active');
	document.querySelector('.score').textContent = sessionStorage.getItem('money');
}

function checkWIn(status) {
	const winCont = document.querySelector('.win-box__body');
	if (status) winCont.classList.add('_visible');
	else if (!status && winCont.classList.contains('_visible')) winCont.classList.remove('_visible');
}

//========================================================================================================================================================
let slot1 = null;

class Slot1 {
	constructor(domElement, config = {}) {
		Symbol1.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel1")).map(
			(reelContainer, idx) =>
				new Reel1(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			//при запуске сбрасываем интервал запуска между слотами
			tl.to(this.spinButton, {});
			if ((+sessionStorage.getItem('money') >= configSlot.bet)) {
				this.spin();
				if (configSlot.isWin) {
					configSlot.isWin = false;
					checkWIn(false);
				}
			} else {
				noMoney('.score');
			}
		});

		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	async spin() {
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()]
		];

		this.onSpinStart(this.nextSymbols);

		await Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin(this.nextSymbols);
			})
		);
	}

	onSpinStart(symbols) {
		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
	}
}

class Reel1 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol1(symbol).img)
		);
	}

	get factor() {
		return 3 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol1(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	async spin(symbols) {
		// запускаем анимацию смещения колонки
		this.param = ((Math.floor(this.factor) * 10) / (3 + Math.floor(this.factor) * 10)) * 100;

		await tl.fromTo(this.symbolContainer, { translateY: 0, }, {
			translateY: `${-this.param}%`,
			duration: configGSAP.duration_1,
			onComplete: () => {

				// определяем какое количество картинок хотим оставить в колонке
				const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

				gsap.to(this.symbolContainer, { translateY: 0, duration: 0 });

				// запускаем цикл, в котором оставляем определенное количество картинок в конце колонки
				for (let i = 0; i < max; i++) {
					this.symbolContainer.firstChild.remove();
				}
			}
		}, '<10%');

		// После выполнения анимации запускаем сценарий разблокировки кнопок и проверки результата
		slot1.onSpinEnd(symbols);
	}
}

const cache1 = {};

class Symbol1 {
	constructor(name = Symbol1.random()) {
		this.name = name;

		if (cache1[name]) {
			this.img = cache1[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-1/slot-${name}.png`;

			cache1[name] = this.img;
		}
	}

	static preload() {
		Symbol1.symbols.forEach((symbol) => new Symbol1(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3'
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config1 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0]) {
			configSlot.isWin = true;
			checkWIn(true);

			let currintWin = configSlot.bet * configSlot.winCoeff_1;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);
			checkLengthScore();
		}
	},
};

if (document.querySelector('.wrapper_game-1')) {
	slot1 = new Slot1(document.getElementById("slot1"), config1);
}

//========================================================================================================================================================
let slot2 = null;
class Slot2 {
	// Создаем стартовую разметку и запускаем слотмашину при клике на кнопку
	constructor(domElement, config = {}) {
		Symbol2.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["2", "3", "4"],
			["5", "6", "1"],
			["3", "4", "5"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["2", "3", "4"],
			["5", "6", "1"],
			["3", "4", "5"],
		];

		this.container = domElement;

		// формируем стартовую раскладку
		this.reels = Array.from(this.container.getElementsByClassName("reel2")).map(
			(reelContainer, idx) =>
				new Reel2(reelContainer, idx, this.currentSymbols[idx])
		);

		// определяем нужные кнопки и пишем логику их работы
		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {

			//при запуске сбрасываем интервал запуска между слотами
			tl.to(this.spinButton, {});

			if ((+sessionStorage.getItem('money') >= configSlot.bet)) {
				this.spin();
				if (configSlot.isWin) {
					configSlot.isWin = false;
					checkWIn(false);
				}
			} else {
				noMoney('.score');
			}
		});

		// не понял для чего
		if (config.inverted) {
			this.container.classList.add("inverted");
		}

		// получаем конфиг игры
		this.config = config;
	}

	// функция запуска слотов
	async spin() {
		// в текущие символы записываем те, которые были созданы в предыдущем запуске как финальные
		// пока не понял для чего
		this.currentSymbols = this.nextSymbols;

		// this.nextSymbols - это символы которые показываем в конце прокрутки, то есть финальные. Здесь мы их создаем
		this.nextSymbols = [
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()]
		];

		// передаем в функцию старта символы, которые хотим отобразить в итоге. 
		this.onSpinStart(this.nextSymbols);

		// возвращаем промис. Он выполняет анимацию прокрутки и когда промис завершен - тогда запускаем функцию onSpinEnd
		await Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin(this.nextSymbols);
			})
		);
	}

	onSpinStart(symbols) {

		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');

		// если хотим что-то делать при старте (обычно ничего не делаю)
		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		// когда анимация завершена, убираем блокировку кнопок
		this.spinButton.classList.remove('_hold');

		// и запускаем логику работы проверки результата, в функции созданной в конфиге. Туда передаем финальный массив
		this.config.onSpinEnd?.(symbols);
	}
}

class Reel2 {
	constructor(reelContainer, idx, initialSymbols) {
		// получаем блок в который записываем символы
		this.reelContainer = reelContainer;
		this.idx = idx;

		// создаем контейнер для символов, даем ему класс
		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		// записываем созданный контейнер в переданный блок. Это будет одна колонка с символами
		this.reelContainer.appendChild(this.symbolContainer);

		// всозданный контейнер вносим картинки переданные при стартовой загрузке
		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol2(symbol).img)
		);
	}

	// величина, которая используется в динамическиъ вычислениях. Насколько понял - как-то подобрана опытным путем
	// величина зависит от номера колонки и чем дальше номер, тем больше величина. Таким образом просто задается небольшой временной демпфер между анимациями разных колонок
	get factor() {
		return 1 + Math.pow(this.idx / 2, 2);
	}

	// этот метод создает промежуточные картинки при запуске анимации
	renderSymbols(nextSymbols) {

		// создаем фрагмент, который представляет собой временный блок который не попадает в дом. Как написано - так лучше для производительности
		const fragment = document.createDocumentFragment();

		// здесь циклом создаем каждой колонке изображения
		// i - задается одинаковая цифра, 3 - работает корректно, с другими не совпадают выпавшие номера и то что записывается внутрь

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			// создаем случайный символ
			const icon = new Symbol2(i >= 10 * Math.floor(this.factor) - 2 ? nextSymbols[i - Math.floor(this.factor) * 10] : undefined);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	async spin(symbols) {

		// запускаем анимацию смещения колонки
		this.param = ((Math.floor(this.factor) * 10) / (3 + Math.floor(this.factor) * 10)) * 100;

		await tl.fromTo(this.symbolContainer, { translateY: 0, }, {
			translateY: `${-this.param}%`,
			duration: configGSAP.duration_2,
			onComplete: () => {

				// определяем какое количество картинок хотим оставить в колонке
				const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

				gsap.to(this.symbolContainer, { translateY: 0, duration: 0 });

				// запускаем цикл, в котором оставляем определенное количество картинок в конце колонки
				for (let i = 0; i < max; i++) {
					this.symbolContainer.firstChild.remove();
				}
			}
		}, '<10%');

		// После выполнения анимации запускаем сценарий разблокировки кнопок и проверки результата
		slot2.onSpinEnd(symbols);
	}
}

const cache2 = {};

class Symbol2 {
	constructor(name = Symbol2.random()) {
		this.name = name;

		if (cache2[name]) {
			this.img = cache2[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-2/slot-${name}.png`;

			cache2[name] = this.img;
		}
	}

	static preload() {
		Symbol2.symbols.forEach((symbol) => new Symbol2(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config2 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] && symbols[2][0] == symbols[3][0] && symbols[3][0] == symbols[4][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] && symbols[2][1] == symbols[3][1] && symbols[3][1] == symbols[4][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2] && symbols[2][2] == symbols[3][2] && symbols[3][2] == symbols[4][2]) {

			configSlot.isWin = true;
			checkWIn(true);

			let currintWin = configSlot.bet * configSlot.winCoeff_2;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);
			checkLengthScore();
		}
	},
};

if (document.querySelector('.wrapper_game-2')) {
	slot2 = new Slot2(document.getElementById("slot2"), config2);
}

