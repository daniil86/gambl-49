import { deleteMoney, addRemoveClass } from "./functions.js";

const preloader = document.querySelector('.preloader');

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	let targetElement = e.target;

	if (targetElement.closest('.header__privacy ') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

})